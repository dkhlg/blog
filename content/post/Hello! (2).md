#** Maandag 11 september 2017**

We hebben vandaag individuele interviews gehouden met diverse studenten. De vragen waren gebaseerd op de enquete die we hadden gemaakt. Hierdoor kunnen we een beter beeld krijgen over de gebruiker van ons app.

Verder hadden we een workshop gehad over Gitlab, dat is een website die voor ons een blog host. Persoonlijk vind ik Gitlab niet een handige programma voor iemand die een blog/logboek wilt starten. Want er is niet echt een overzichtelijke dashboard naar mijn mening. Maar na een aantal keer navragen bij docent is het me toch gelukt om de basis onder de knie te krijgen.