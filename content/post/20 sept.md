


LOGBOEK
> *Woensdag 20 september 2017*

Vandaag ben ik aan de slag gegaan met de tweede iteratie, daarbij heb ik de taakverdeling gemaakt en ook een start gemaakt met het brainstormen over de applicatie. De taakverdeling maken was iets waar ik me deze iteratie beter op wilde focussen, ik heb veel geleerd van de eerste iteratie en het feit dat een goede voorbereiding en taakverdeling het halve werk is. Tijdens de eerste iteratie deed iedereen uit mijn groep eigenlijk vanalles wat er werd geroepen en niet specifiek iets wat bijvoorbeeld prioriteit had, dit zijn situaties die ik wilde voorkomen in de tweede iteratie. Mijn groep weet op dit moment waar die aan toe is en iedereen heeft een start gemaakt met de voorbereidingen.

De laatste 2 uren van de design challenge waren gericht op het leren kennen van je klasgenoten en je tekenskills. We kregen diverse woorden die we moesten uit tekenen, dit proces ging bij mij moeizaam omdat mijn teken skills nog onder niveau zijn (ook al is dit geen vereiste). Ik kijk er naar uit opdrachten te maken die mijn skills zullen opschroeven, uiteraard ga ik zelf ook aan het werk buiten school om aan dit probleem te werken.

Verder hebben gewerkt aan de blog, nog steeds moet ik zeggen dat ik het systeem niet fijn vindt en niet heel veel heb kunnen uitvoeren. Ik ga dit probleem volgende week proberen te verhelpen doormiddel van het kijken van youtube videos.  